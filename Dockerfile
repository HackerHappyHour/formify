FROM node:10-alpine

ENV FORMIFY_SCHEME https

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN npm install

COPY src/*.js src/

RUN chown -R node:node /usr/src/app
USER node:node

CMD ["npm", "start"]
