# Formify

Create user-friendly HTML Web Forms out of your GitLab or GitHub issue templates

## Dev Setup
* __Note:__ Refer to `Settings` -> `CI/CD` -> `Variables` for values.
```
export FORMIFY_APP_ID=<id value>
export FORMIFY_APP_SECRET=<secret value>
```
* The redirect url for `FORMIFY_LOCAL_APP_ID` is set to:
`http://localhost:3000/connect/gitlab/callback`

* The redirect url for `FORMIFY_APP_ID` is set to:
`http://formify.nshine.org/connect/gitlab/callback`

__Alternatively:__ [create your own Application][applications] credentials in User Settings -> Applications.

## Run

```
npm run dev
```

Navigate to `http://localhost:3000`

## Run Docker

```
docker-compose up -d
```

or:

```
docker build -t formify .
docker run -p 3000:3000 -e FORMIFY_APP_ID -e FORMIFY_APP_SECRET formify
```

## Run Prod
```
docker run -d --restart always -p 80:80 -p 443:443 -e FORMIFY_APP_ID -e FORMIFY_APP_SECRET -e FORMIFY_HOST -e FORMIFY_SCHEME -e FORMIFY_DNS_CHALLENGE_KEY --sysctl net.ipv4.ip_unprivileged_port_start=0 registry.gitlab.com/hacherhappyhour/formify:poc-nick
```

---

## Reference

* [Koa][koa]
* [Grant][grant]
* [Greenlock][greenlock]

[applications]:https://gitlab.com/profile/applications
[grant]:https://github.com/simov/grant
[koa]:https://koajs.com/
[greenlock]: https://www.npmjs.com/package/greenlock-express
[greenlock-koa]:https://git.coolaj86.com/coolaj86/greenlock-koa.js
