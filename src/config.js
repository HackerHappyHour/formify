import { homedir } from 'os';
/*
const leChallengeDigitalOcean = require('le-challenge-digitalocean').create({
  debug: true,
  doApiKey: process.env.FORMIFY_DNS_CHALLENGE_KEY,
});
 */
const http01 = require('le-challenge-fs').create({ webrootPath: '/tmp/acme-challenges' });

const home = homedir();

const oAuthConfig = {
  server: {
    protocol: process.env.FORMIFY_SCHEME || 'http',
    host: process.env.FORMIFY_HOST || 'localhost:3000',
    callback: '/callback',
    transport: 'session',
    state: true,
  },
  gitlab: {
    key: process.env.FORMIFY_APP_ID,
    secret: process.env.FORMIFY_APP_SECRET,
    scope: ['read_user'],
    // scope: ['api', 'read_user'],
  },
};

const tlsConfig = {
  version: 'draft-11',
  server: 'https://acme-v02.api.letsencrypt.org/directory',
  email: 'shine.nick@gmail.com',
  agreeTos: true,
  approveDomains: ['formify.nshine.org'],
  challengeType: 'http-01',
  challenges: {
    // 'dns-01': leChallengeDigitalOcean,
    'http-01': http01,
  },
  communityMember: false,
  configDir: `${home}/acme/etc`,
  debug: true,
};

export {
  oAuthConfig,
  tlsConfig,
};
