import Koa from 'koa';
import http from 'http';
import https from 'https';
import enforceHttps from 'koa-sslify';
import router from 'koa-route';
import session from 'koa-session';
import mount from 'koa-mount';
import grant from 'grant-koa';
import reqLogger from 'koa-logger';
import Gitlab from 'gitlab';

import { oAuthConfig, tlsConfig } from './config';
import logger from './logger';

const app = new Koa();
app.use(reqLogger());

app.keys = ['grant'];
app.use(session(app));

app.use(mount(grant(oAuthConfig)));

app.use(router.get('/', async (ctx) => {
  ctx.redirect('/connect/gitlab');
}));

app.use(router.get('/callback', async (ctx) => {
  if (ctx.session.isNew || !ctx.session.grant) {
    ctx.redirect('/connect/gitlab');
  }

  logger.info(ctx.session.grant.response);


  const n = ctx.session.views || 0;
  ctx.session.views = n + 1;

  const accessToken = ctx.session.grant.response.access_token;
  const gitlab = new Gitlab({
    oauthToken: accessToken,
  });

  const currentUser = await gitlab.Users.current();

  ctx.body = `
    session views: ${ctx.session.views}
    authenticated user: ${currentUser.name}

    ${JSON.stringify(currentUser, null, 2)}
  `;
}));


const greenlock = require('greenlock-express').create(tlsConfig);
// temporary tls until K8s and nginx reverse proxy
// if (process.env.NODE_ENV === 'production') {
if (process.env.NODE_ENV === 'nothing') {
  const redirectHttps = app.use(enforceHttps()).callback();

  const httpServer = http.createServer(greenlock.middleware(redirectHttps));
  httpServer.listen(80, () => {
    logger.info(`Koa server listening on port 80 to handle ACME http-01
    challenge and redirect to https`);
  });

  const server = https.createServer(greenlock.tlsOptions, greenlock.middleware(app.callback()));
  server.listen(443, () => logger.info('Koa server listening on port 443'));
} else {
  const port = 3000;
  app.listen(port, () => logger.info(`Koa server listening on port ${port}`));
}
